import { Heading } from '@chakra-ui/react'
import React from 'react'

const Title = ({children}) => {
  return (
    <Heading as='h1' size='2xl' noOfLines={1}>{children}</Heading>
  )
}

export default Title