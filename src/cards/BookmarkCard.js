import { Avatar, HStack, Text, VStack } from '@chakra-ui/react'
import React from 'react'

const BookmarkCard = ({name, url, onDelete}) => {
  return (
    <HStack onClick={() => {
        window.open(url, "_blank");
    }} padding={"24px"} width={"100%"} backgroundColor={"#9681EB"} borderRadius={"8px"}>
        <Avatar
            boxSize='100px'
            objectFit='cover'
            name={name}
        />
        <VStack marginLeft={"12px"} alignItems={"start"} flex={2}>
            <Text fontSize='2xl'>{name}</Text>
            <Text fontSize='md'>{url}</Text>
        </VStack>
        <VStack marginLeft={"12px"} alignItems={"end"} flex={0.5} onClick={(e) => {
            e.stopPropagation();
            if (onDelete) {
                onDelete()
            }
        }}>
            <Text textAlign={"center"} fontSize='xl'>{"x"}</Text>
        </VStack>
    </HStack>
  )
}

export default BookmarkCard