import { VStack } from '@chakra-ui/react'
import React from 'react'

const BaseVerticalStack = ({children}) => {
  return (
    <VStack
      bg={"#000000"}
      padding={"36px 24px"}
      minH={"100vh"}
    >{children}</VStack>
  )
}

export default BaseVerticalStack