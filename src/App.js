import { useRecoilState } from "recoil";
import BookmarkCard from "./cards/BookmarkCard";
import BaseVerticalStack from "./layouts/BaseVerticalStack";
import Space from "./texts/Space";
import Title from "./texts/Title";
import { bookmarksState } from "./states/bookmarks";
import { Button, Divider, HStack, Input } from "@chakra-ui/react";
import { useState } from "react";
import EmptyImage from "./assets/empty-box.png"

function App() {
  const [bookmarks, setBookmarks] = useRecoilState(bookmarksState)
  const [name, setName] = useState("")
  const [url, setUrl] = useState("")

  const addBookmark = () => {
    let list = [];
    let newBookmark = {
      id: 2000000000000 - Date.now(),
      name,
      url
    }
    list.push(newBookmark)
    bookmarks.bookmarks.forEach(element => {
      list.push(element)
    });
    setBookmarks({
      bookmarks: list
    })
    setTimeout(() => {
      setName("")
      setUrl("")
    }, 100);
  }

  const removeBookmark = (id) => () => {
    let list = [];
    bookmarks.bookmarks.forEach(element => {
      if (element.id !== id) {
        list.push(element)
      }
    });
    setBookmarks({
      bookmarks: list
    })
  }

  return (
    <BaseVerticalStack>
      <Title>Bukmarq</Title>
      <Space size={6}/>
      <p style={{textAlign: "center"}}>An alternative Bookmarking tool for your browser.</p>
      <Space size={24}/>
      <Input placeholder='Name' value={name} onChange={(e) => setName(e.target.value)} />
      <Space size={8}/>
      <Input placeholder='Url' value={url} onChange={(e) => setUrl(e.target.value)}/>
      <Space size={8}/>
      <Button width={"100%"} colorScheme='blue' onClick={addBookmark}>Add</Button>
      <Space size={24}/>
      <HStack width={"100%"}>
        <Divider />
        <p width={"400px"}>Bookmarks</p>
        <Divider />
      </HStack>
      <Space size={8}/>
      {bookmarks.bookmarks.map((data) => {
        return (
          <BookmarkCard
            key={data.id}
            name={data.name}
            url={data.url}
            onDelete={removeBookmark(data.id)}
          />
        )
      })}
      {bookmarks.bookmarks.length === 0 && (
        <img style={{height: "200px", width: "200px", marginTop: "80px"}} src={EmptyImage} alt=""/>
      )}
    </BaseVerticalStack>
  );
}

export default App;
