import { atom } from "recoil";
import { recoilPersist } from "recoil-persist";

const { persistAtom } = recoilPersist();

export const bookmarksState = atom({
    key: "bookmarksState",
    default: {
        bookmarks: []
    },
    effects_UNSTABLE: [persistAtom]
});
